﻿using System.Collections.Generic;

namespace HashCode
{
    public class Car
    {
        public int Id { get; set; }
        public Coordinate CurrentLocation { get; set; }
        public Coordinate Destination { get; set; }
        public bool IsAvailable { get; set; }
        public bool GoingToStartOfTheRide { get; set; }
        public bool GoingToEndOfTheRide { get; set; }
        public int Counter { get; set; }
        public IList<Ride> Rides { get; set; }

        public override string ToString()
        {
            return "Car " + Id + ", current location: " + CurrentLocation.Row + ", " + CurrentLocation.Col + ", is available=" + IsAvailable + ", go to start=" + GoingToStartOfTheRide + ", go to end" + GoingToEndOfTheRide;
        }

        public Car()
        {
            Rides = new List<Ride>();
            CurrentLocation = new Coordinate {Row = 0, Col = 0};
            Destination = new Coordinate {Row = 0, Col = 0};
            IsAvailable = true;
            GoingToEndOfTheRide = false;
            GoingToStartOfTheRide = false;
            Counter = 0;
        }

        public string FinishedRides()
        {
            var finishedRides = "";
            foreach (Ride ride in Rides)
            {
                if (ride.IsFinished)
                {
                    var rideId = ride.Id + " ";
                    finishedRides += rideId;
                }
            }
            return "Car id " + Id + " Done rides: " + finishedRides;
        }

        public string GenerateResult()
        {
            var finishedRides = " ";
            foreach (Ride ride in Rides)
            {
                if (ride.IsFinished && ride.IsInProgress)
                {
                    var rideId = ride.Id + " ";
                    finishedRides += rideId;
                }
            }

            // for d & d
            //return Rides.Count - 1 + finishedRides;
            return Rides.Count + finishedRides;
        }
    }
}