﻿namespace HashCode
{
    public class RideInProgress
    {
        public Car Car { get; set; }
        public Ride Ride { get; set; }
    }
}