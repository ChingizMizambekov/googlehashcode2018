﻿using System;

namespace HashCode
{
    public class Ride
    {
        public int Id { get; set; }
        public Coordinate StartCordinate { get; set; }
        public Coordinate EndCoordinate { get; set; }
        public int StartStep { get; set; }
        public int EndStep { get; set; }
        public bool IsStarted { get; set; }
        public bool IsInProgress { get; set; }
        public bool IsFinished { get; set; }

        public string StartCordinateToString()
        {
            return "Start coordinate: " + StartCordinate.Row + ", " + StartCordinate.Col;
        }

        public string EndCoordinateToString()
        {
            return "End coordinate: " + EndCoordinate.Row + ", " + EndCoordinate.Col;
        }

        public override string ToString()
        {
            return "Start step " + StartStep + ", end step: " + EndStep + ", " + StartCordinateToString() + ", " + EndCoordinateToString();
        }

        public Ride()
        {
            IsStarted = false;
            IsFinished = false;
            IsInProgress = false;
        }
    }
}