﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HashCode
{
    class Program
    {
        private static string[] dataSet;
        private static City city;
        private static List<Ride> rides = new List<Ride>();
        private static List<Car> cars = new List<Car>();
        private static int carIndex = 0;
        private static int rideIndex = 0;
        private static int stepsCounter = 0;
        private static bool anyRideLeft = true;
        static int onTime = 0;
        static int NotOnTime = 0;
        //private static string fileName = "a_example.in";
        //private static string fileName = "b_should_be_easy.in";
        //private static string fileName = "c_no_hurry.in";
        //private static string fileName = "d_metropolis.in";
        private static string fileName = "e_high_bonus.in";

        static void Main(string[] args)
        {
            #region initialize

            
            dataSet = File.ReadAllLines(@"C:\projects\Quadzero\HashCode\HashCode\DataSet\" + fileName);
            for (int i = 0; i < dataSet.Length; i++)
            {
                var cityData = dataSet[i].Split(' ');
                if (i == 0)
                {
                    city = new City
                    {
                        Rows = Convert.ToInt32(cityData[0]),
                        Cols = Convert.ToInt32(cityData[1]),
                        Cars = Convert.ToInt32(cityData[2]),
                        Rides = Convert.ToInt32(cityData[3]),
                        Steps = Convert.ToInt32(cityData[5])
                    };
                }
                else
                {
                    var ride = new Ride
                    {
                        Id = i - 1,
                        StartCordinate = new Coordinate
                        {
                            Row = Convert.ToInt32(cityData[0]),
                            Col = Convert.ToInt32(cityData[1])
                        },
                        EndCoordinate = new Coordinate
                        {
                            Row = Convert.ToInt32(cityData[2]),
                            Col = Convert.ToInt32(cityData[3])
                        },
                        StartStep = Convert.ToInt32(cityData[4]),
                        EndStep = Convert.ToInt32(cityData[5])
                    };
                    rides.Add(ride);
                }
            }

            
            for (int j = 0; j < city.Cars; j++)
            {
                var car = new Car
                {
                    Id = j + 1
                };
                cars.Add(car);
            }
            #endregion
            //DebugInfo();

            rides = rides.OrderBy(x => x.StartStep).ToList();

            // while we have steps over and there are open rides in the list
            while (city.Steps > 0 || !anyRideLeft)
            {
                // assign a ride to a car on a one-to-one basis if the car is available, dont bother about any condition yet
                for (var i = 0; i < cars.Count; i++)
                {
                    // select first car
                    var car = cars[carIndex];

                    // check if it is available
                    if (car.IsAvailable)
                    {
                        var ride = GetNextRide();

                        // if it is, check if there is any open ride in the list 
                        if (ride != null)
                        {
                            // assign first ride from the list to the car



                            // Check if the ride can be started
                            if (car.CurrentLocation.Row == ride.StartCordinate.Row &&
                                car.CurrentLocation.Col == ride.StartCordinate.Col)
                            {
                                car.GoingToEndOfTheRide = true;
                                car.GoingToStartOfTheRide = false;
                                ride.IsStarted = true;
                            }
                            else
                            {
                                car.GoingToEndOfTheRide = false;
                                car.GoingToStartOfTheRide = true;
                                ride.IsStarted = false;
                            }

                            car.Rides.Add(ride);
                            car.IsAvailable = false;
                        }
                    }
                    // if the car is not available, move it with 1 step start/end location ~ ride started or not
                    Move(car);
                    if (carIndex == cars.Count - 1)
                    {
                        carIndex = 0;
                    }
                    else
                    {
                        carIndex++;
                    }
                }

                city.Steps--;
                stepsCounter++;
                //DebugInfo();
                AnyRideLeft();
                Console.WriteLine(stepsCounter);
            }


            GenerateResultFile();
            Console.Read();
        }

        private static void GenerateResultFile()
        {
            List<string> result = new List<string>();
            foreach (Car car in cars)
            {
                result.Add(car.GenerateResult());
            }
            File.WriteAllLines(@"C:\projects\Quadzero\HashCode\HashCode\DataSet\output_" + fileName, result);
            Console.WriteLine(result);
        }

        private static void AnyRideLeft()
        {
            foreach (Ride ride in rides)
            {
                if (!ride.IsFinished) anyRideLeft = true;
                return;
            }

            anyRideLeft = false;
        }

        private static Ride GetNextRide()
        {
            foreach (var ride in rides)
            {
                if (!ride.IsInProgress)
                {
                    ride.IsInProgress = true;
                    return ride;
                }
            }
            return null;
        }


        private static void Move(Car car)
        {
            var currentRide = car.Rides[car.Rides.Count - 1];
            var isRideStarted = currentRide.IsStarted && car.GoingToEndOfTheRide && !currentRide.IsFinished;
            // if ride is not started yet, move to the start position of the ride
            if (!car.IsAvailable)
            {
                if (!isRideStarted)
                {
                    MoveToStart(car);
                }
                // move to the end position of the ride
                else
                {
                    MoveToEnd(car);
                }
            }
        }

        private static void MoveToStart(Car car)
        {
            var currentRide = car.Rides[car.Rides.Count - 1];
            if (car.CurrentLocation.Row < currentRide.StartCordinate.Row)
            {
                car.CurrentLocation.Row++;
            }
            else if (car.CurrentLocation.Row > currentRide.StartCordinate.Row)
            {
                car.CurrentLocation.Row--;
            }
            else if (car.CurrentLocation.Col > currentRide.StartCordinate.Col)
            {
                car.CurrentLocation.Col--;
            }
            else if (car.CurrentLocation.Col < currentRide.StartCordinate.Col)
            {
                car.CurrentLocation.Col++;
            }

            if (car.CurrentLocation.Row == currentRide.StartCordinate.Row &&
                car.CurrentLocation.Col == currentRide.StartCordinate.Col)
            {
                car.GoingToEndOfTheRide = true;
                car.GoingToStartOfTheRide = false;
                car.Rides[car.Rides.Count - 1].IsStarted = true;
            }
        }

        private static void MoveToEnd(Car car)
        {
            var currentRide = car.Rides[car.Rides.Count - 1];
            if (car.CurrentLocation.Row < currentRide.EndCoordinate.Row)
            {
                car.CurrentLocation.Row++;
            }
            else if (car.CurrentLocation.Row > currentRide.EndCoordinate.Row)
            {
                car.CurrentLocation.Row--;
            }
            else if (car.CurrentLocation.Col > currentRide.EndCoordinate.Col)
            {
                car.CurrentLocation.Col--;
            }
            else if (car.CurrentLocation.Col < currentRide.EndCoordinate.Col)
            {
                car.CurrentLocation.Col++;
            }

            if (car.CurrentLocation.Row == currentRide.EndCoordinate.Row &&
                car.CurrentLocation.Col == currentRide.EndCoordinate.Col)
            {
                car.GoingToEndOfTheRide = false;
                car.GoingToStartOfTheRide = false;
                car.Rides[car.Rides.Count - 1].IsStarted = false;
                car.Rides[car.Rides.Count - 1].IsFinished = true;
                car.IsAvailable = true;
            }
        }



        private static void DebugInfo()
        {
            Console.WriteLine("Step: " + stepsCounter);
            Console.WriteLine(city.ToString());
            Console.WriteLine("Cars array length: " + cars.Count);
            Console.WriteLine("Rides array length: " + rides.Count);
            foreach (Car car in cars)
            {
                Console.Write(car + "\t");
                Console.WriteLine(car.FinishedRides());
            }

            foreach (Ride ride in rides)
            {
                Console.WriteLine(ride.ToString());
            }

            Console.WriteLine("================================================================================\n");
        }
    }
}
