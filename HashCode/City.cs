﻿namespace HashCode
{
    public class City
    {
        public int Rows { get; set; }
        public int Cols { get; set; }
        public int Cars { get; set; }
        public int Rides { get; set; }
        public int Steps { get; set; }

        public override string ToString()
        {
            return "Rows " + Rows + ", Cols " + Cols + ", Cars " + Cars + ", Rides " + Rides + ", Steps " + Steps;
        }
    }
}